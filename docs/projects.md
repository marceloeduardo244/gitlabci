---
id: projects
title: Lista de projetos
---

<ul>
<h4>DevOps</h4>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/email-worker-compose">Email worker | Docker Compose</a>
</li>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/primeira-imagem-composta-docker">
Imagem Docker Compose simples</a>
</li>
<li>
<a target="_blank" href="https://gitlab.com/marceloeduardo244/gitlabci">
Meu Portifólio | Fluxo completo de CI & CD</a>
</li>
<h4>Front-End</h4>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/clone-instagran">Clone Instagram | ReactJS</a>
</li>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/clone-netflix-react">Clone Netflix | ReactJS</a>
</li>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/gobarber-web">GoBarber | Rocketseat | ReactJS</a>
</li>
<h4>Back-End</h4>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/api-spring-boot-with-jpa-hibernate">API | Java | Spring-Boot | JPA</a>
</li>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/curso-go-web-avancado">API | Golang</a>
</li>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/backend-gobarber">Gobarber | NodeJS</a>
</li>
<h4>Mobile</h4>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/goRestaurantMobile">GoRestaurant | Rocketseat | React-Native</a>
</li>
<li>
<a target="_blank" href="https://github.com/marcelosilva-dev/mobile-gobarber">GoBarber | Rocketseat | React-Native</a>
</li>
</ul>
