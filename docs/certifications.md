---
id: certifications
title: Lista de certificações
---

<ul>
<h4>DevOps</h4>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-8fa5013b-3ef4-4529-b6ca-3845f1e1fa64/">Docker</a>
</li>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-b35b7df2-0b1f-415d-b274-3418e14febea/">Gitlab CI & CD | Pipelines</a>
</li>
<h4>Front-End</h4>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-87e69edb-e1fc-424f-b9d6-f5f43088fb6a/">VueJS</a>
</li>
<li>
<a target="_blank" href="https://app.rocketseat.com.br/api/certificates/pdf/908993b8-aa7f-452b-babe-65c8d5cd215f">ReactJS | React-Native</a>
</li>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-4YVP1SI9/">Desenvolvimento Web</a>
</li>
<h4>Back-End</h4>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-0d4feb27-f160-493c-8570-ebbb0440ffbb/">Golang</a>
</li>
<li>
<a target="_blank" href="https://app.rocketseat.com.br/api/certificates/pdf/908993b8-aa7f-452b-babe-65c8d5cd215f">NodeJS</a>
</li>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-df666cc0-4ca7-40f2-a845-c102f3d06525/">Java</a>
</li>
<h4>Fluxo e organização</h4>
<li>
<a target="_blank" href="https://c46e136a583f7e334124-ac22991740ab4ff17e21daf2ed577041.ssl.cf1.rackcdn.com/Certificate/ScrumFundamentalsCertified-MarceloOliveira-877403.pdf">Scrum</a>
</li>
<li>
<a target="_blank" href="https://www.udemy.com/certificate/UC-EF9404BI/">Planejamento e Gestão de projetos</a>
</li>
</ul>
